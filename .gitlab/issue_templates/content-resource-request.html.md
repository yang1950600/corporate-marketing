## Details

<!-- all fields must be completed before moving issue to status:wip --> 

### Content title:

* **Content DRI:**
* **MPM DRI:**
* **PMM DRI:**
* **Pillar:** 
* **Campaign:**
* **Target audience:**
* **Target keyword:**

#### Copy due date:
#### Launch date:

## Summary 

*What is this resource about?*

<!--- Fill this out before submitting -->

*What should readers walk away with?* 

<!--- Fill this out before submitting -->

*Research requirements?*

<!--- Fill this out before submitting -->

### Outline

1. Key point 1
1. Key point 2
1. Key point 3

## TODO 

* [ ] Add a deadline
* [ ] Summary & outline approved by Content Marketing
* [ ] Assign author
* [ ] Write draft
* [ ] Get keyword input from Digital team
* [ ] SME review 
* [ ] Move to MR for final review and publishing
`Close this issue!`

/label  ~"Content Marketing" ~"status::plan" ~"MPM - Radar"

/assign @erica 