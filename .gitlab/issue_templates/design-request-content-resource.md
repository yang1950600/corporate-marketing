# DETAILS

- **Content owner:** 
- **Content title:** 
- **Final copy draft delivery date:** 
- **First PDF draft delivery date:**
- **Requested final PDF delivery date:** 
- **Notes for designer:**

### [Resource draft](#) 

# ACTION ITEMS

- [ ] Final copy shared with the designer `(@content-owner)` 
- [ ] Graphics confirmed (charts, images, etc.) with the designer `(@production-designer)` 
- [ ] First PDF draft delivered `(@production-designer)` 
- [ ] Content owner review `(@content-owner)` 
- [ ] Final PDF delivered `(@production-designer)` 
- [ ] Final review `(@content-owner)` 
- [ ] PDF link shared to gated-content issue `(@production-designer)`

/assign @luke 

/label ~Design 